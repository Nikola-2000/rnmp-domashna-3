# RNMP Domashna 3

Проектот е на master branch. Овој фајл го сменив после 12 се надевам дека е ОК.

За целата домашна да биде извршена треба во main.py да се повика најпрво offline методот преку d објектот, и потоа да се повика producer.py за да се испратат на Kafka online.csv податоците и после во main.py се повикува методот produce_and_consume


util.py - Оваа скрипта е offline фазата

    -За да се подели множеството се повикува split_csv методот кој ги зачувува локално користејќи го train_test_split од sklearn. offline методот е главниот метод каде се одвива целата фаза. Најпрво се чита offline.csv и се повикува transform методот каде што множеството се пополнуваат null вредностите со нула и се скалираат со StandardScaler. За да се скалираат податоците потребно е да се претворат сите колони во Vector и тоа е една колона сега. Излезниот dataframe изгледа како "label" "scaledFeatures". Потоа се тренираат 3 модели ( LogisticRegression, RandomForestClassifier и Gradient-Boosted Trees Classifier) каде се тунирани со CrossValidator. Потоа со MulticlassClassificationEvaluator што ја користи ф1 метриката одредуваме кој модел е најдобар и го зачувуваме. 
    -load_model() se koristi za loadiranje na modelot
    -predict_and_consume() vo ovoj metod se load-nuva modelot i baraniot dataframe od kafka topic. Rezultatite bidejkji se vo json format se pretvaraat vo dataframe bidejkji modelite bea fit-nati so dataframe. Se transformiaat podatocite za da moze da se ispratat na modelot. Se pravat predviduvanja na toj dataframe i potoa se prakjaat na kafka topic


main.py

    -Најдобриот модел се зема со методот load_model и се зема шемата на dataframe-ot за да може да се прочита од Кафка. Од Кафка читаме со readStream.format("kafka").option("kafka.bootstrap.servers", "localhost:9092").option("startingOffsets", "earliest").option("subscribe", "health_data").load(). Потоа се користи истата функција за трансформирање и потоа со write се праќа на Кафак топик со име health_data_predicted
    
producer.py

    -Во оваа скрипта најпрво го читам online.csv со pandas пакетот. Потоа секој ред го претварам во json формат и се испраќа на Kafka topic-от со име health_data.

